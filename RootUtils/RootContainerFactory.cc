/*
        \file                          RootContainerFactory.cc
        \brief                         Container factory for DQM
        \author                        Fabio Ravera, Lorenzo Uplegger
        \version                       1.0
        \date                          14/06/19
        Support :                      mail to : fabio.ravera@cern.ch

 */

#include "../RootUtils/RootContainerFactory.h"
#include "../Utils/Container.h"
